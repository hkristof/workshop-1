const argv = require('yargs').argv

let bet = argv.bet;
let balance = argv.balance;
let verbose = argv.verbose;
let stopWin = argv.stopWin;
let zero = argv.zero;

let startBet = bet;
let startBalance = balance;

let rounds = 1;
const red = [1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36];
const black = [2, 4, 5, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35];
let log = [];
let stopLimit = 1.2;
if(stopWin)
{
    let stopWinLimit = stopWin.split("%")[0];
    stopLimit = stopWinLimit / 100 + 1;
}
let loseCondition = 0;
let winCondition = Math.floor(startBalance * stopLimit);

log.push(`Game start | balance: ${balance}, bet: ${bet}`);

while(balance > loseCondition && balance < winCondition)
{
    balance = balance - bet;
    const spinResult = getRandomIntInclusive(0, 36);
    log.push(`Round ${rounds} spin: ${spinResult}`);
    if(zero)
    {
        if(spinResult === 0)
        {
            balance = balance + bet * 35;
            bet = startBet;
            log.push(`Round ${rounds} won | balance: ${balance}, bet: ${bet}`);
        }
        else
        {
            if(bet * 2 > balance)
            {
                bet = balance;
            }
            else
            {
                bet = bet * 2;
            }
            log.push(`Round ${rounds} lost | balance: ${balance}, bet: ${bet}`);
        }
    }
    else
    {
        if(red.includes(spinResult))
        {
            balance = balance + bet * 2;
            bet = startBet;
            log.push(`Round ${rounds} won | balance: ${balance}, bet: ${bet}`);
        }
        else
        {
            if(bet * 2 > balance)
            {
                bet = balance;
            }
            else
            {
                bet = bet * 2;
            }
            log.push(`Round ${rounds} lost | balance: ${balance}, bet: ${bet}`);
        }
    }

    rounds++;
}
if(verbose)
{
    log.forEach(i => console.log(i));
}
if(balance <= loseCondition)
{
    console.log(`Lost in ${rounds} rounds!`);
}
else
{
    console.log(`Won in ${rounds} rounds!`);
}
rounds = 0;
log = [];


function getRandomIntInclusive(min, max)
{
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive
}